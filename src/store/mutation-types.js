export const TOGGLE_SIDE_MENU = 'TOGGLE_SIDE_MENU'
export const SELECT_VISUALIZATION_MODE = 'SELECT_VISUALIZATION_MODE'
export const GET_VISUALIZATION_SIZE = 'GET_VISUALIZATION_SIZE'
export const PLAY = 'PLAY'
export const STOP = 'STOP'
export const SELECT_PALO = 'SELECT_PALO'
export const SELECT_TEMPO = 'SELECT_TEMPO'
export const SELECT_CROCHETS = 'SELECT_CROCHETS'
export const SELECT_INSTRUMENTS = 'SELECT_INSTRUMENTS'
export const SELECT_PRECOUNT = 'SELECT_PRECOUNT'
export const SELECT_STARTBEAT = 'SELECT_STARTBEAT'
export const CHANGE_VOLUME = 'CHANGE_VOLUME'
export const TOGGLE_EIGHTHNOTES = 'TOGGLE_EIGHTHNOTES'
export const ENABLE_EIGHTHNOTES = 'ENABLE_EIGHTHNOTES'
export const DISABLE_EIGHTHNOTES = 'DISABLE_EIGHTHNOTES'
export const TOGGLE_IMPROVISE = 'TOGGLE_IMPROVISE'
export const ENABLE_IMPROVISE = 'ENABLE_IMPROVISE'
export const DISABLE_IMPROVISE = 'DISABLE_IMPROVISE'
export const TOGGLE_HUMANIZE = 'TOGGLE_HUMANIZE'
export const ENABLE_HUMANIZE = 'ENABLE_HUMANIZE'
export const DISABLE_HUMANIZE = 'DISABLE_HUMANIZE'
export const SHOW_SLOW_MESSAGE = 'SHOW_SLOW_MESSAGE'
export const HIDE_SLOW_MESSAGE = 'HIDE_SLOW_MESSAGE'
export const SHOW_FAST_MESSAGE = 'SHOW_FAST_MESSAGE'
export const HIDE_FAST_MESSAGE = 'HIDE_FAST_MESSAGE'
export const TRIGGER_EVENT = 'TRIGGER_EVENT'
export const TOGGLE_TRACKVISITS = 'TOGGLE_TRACKVISITS'
export const ENABLE_TRACKVISITS = 'ENABLE_TRACKVISITS'
export const DISABLE_TRACKVISITS = 'DISABLE_TRACKVISITS'
export const INITIALIZE_TRACKING = 'INITIALIZE_TRACKING'
export const ENABLE_TRACKINGCHOSEN = 'ENABLE_TRACKINGCHOSEN'
export const OPEN_PRIVACYDIALOG = 'OPEN_PRIVACYDIALOG'
export const CLOSE_PRIVACYDIALOG = 'CLOSE_PRIVACYDIALOG'
