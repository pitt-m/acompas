export default {
  clara: [
    {
      src: 'clara/clara_1',
      volume: 0
    },
    {
      src: 'clara/clara_2',
      volume: 0
    },
    {
      src: 'clara/clara_3',
      volume: -2
    }
  ],
  sorda: [
    {
      src: 'sorda/sorda_1',
      volume: -2
    },
    {
      src: 'sorda/sorda_2',
      volume: -4
    },
    {
      src: 'sorda/sorda_3',
      volume: -6
    }
  ],
  cajon: [
    {
      src: 'cajon/cajon_1',
      volume: -8
    },
    {
      src: 'cajon/cajon_2',
      volume: -6
    },
    {
      src: 'cajon/cajon_3',
      volume: -6
    }
  ],
  udu: [
    {
      src: 'udu/udu_1',
      volume: 0
    },
    {
      src: 'udu/udu_2',
      volume: 0
    },
    {
      src: 'udu/udu_3',
      volume: 0
    }
  ],
  jaleo: [
    {
      src: 'jaleo/jaleo_1',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_2',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_3',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_4',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_5',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_6',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_7',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_8',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_9',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_10',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_11',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_12',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_13',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_14',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_15',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_16',
      volume: -4
    },
    {
      src: 'jaleo/jaleo_17',
      volume: -4
    }
  ],
  click: [
    {
      src: 'click/click_1',
      volume: -9
    },
    {
      src: 'click/click_2',
      volume: -9
    }
  ]
}
